const { driver } = require('@rocket.chat/sdk')

class RocketChat {
  constructor(config) {
    const { host, username, password, ssl, prefix } = config
    this.host = host
    this.username = username
    this.password = password
    this.useSsl = ssl
    this.prefix = prefix
    this.userId = null
  }

  async connect() {
    const _conn = await driver.connect({ host: this.host, useSsl: this.useSsl })
    this.userId = await driver.login({ username: this.username, password: this.password })
    await driver.subscribeToMessages()
  }

  async joinRoom(roomId) {
    await driver.joinRoom(roomId)
  }

  async sendToRoom(roomId, message) {
    return await driver.sendToRoomId(message, roomId)
  }

  setMessageHandler(handler) {
    driver.reactToMessages(async (err, message, messageOptions) => {
      if ( err ) return
      if ( message.u._id === this.userId ) return
      if ( !message.msg.toLowerCase().startsWith(this.prefix) ) return
      const msgWithoutPrefix = message.msg.substr(this.prefix.length + 1)
      const roomId = message.rid
      const reply = async (response) => {
        return await this.sendToRoom(roomId, response)
      }

      // add botmsg field
      message.botmsg = msgWithoutPrefix

      handler(message, reply)
    })
  }
}

module.exports = RocketChat
