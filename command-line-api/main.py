from flask import Flask
from flask import jsonify
import subprocess
app = Flask(__name__)
 
@app.route("/")
def hello():
    return "Hello World!"

@app.route("/execute")
def execute():
    command = ['docker','run', "-d", "-p0:27017", "mongo"]
    p = subprocess.Popen(command, stdout=subprocess.PIPE)
    p.wait()
    p2 = subprocess.Popen(['docker', 'container', 'ls', '--latest'], stdout=subprocess.PIPE)
    p2.wait()
    resp = jsonify(success=True, message=p2.stdout.read().decode("utf-8"))
    return resp

if __name__ == "__main__":
    app.run(host='0.0.0.0')
