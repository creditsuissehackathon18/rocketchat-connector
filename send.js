const config = require('./config.json')
const RocketChat = require('./RocketChat')

const runbot = async () => {
  const rocketChat = new RocketChat(config)
  await rocketChat.connect()
  console.log('CONNECTED!')

  await rocketChat.joinRoom('GENERAL')

  const attachments = [{
    "collapsed": true,
    "color": "#ff0000",
    "title": "HKD 38,631,152.62",
    "author_name": "Total Wealth",
    "fields": [{
      "short": true,
      "title": "Month-to-Date",
      "value": "-0.29% | HKD -114,163.57"
    },
    {
      "short": true,
      "title": "Quarter-to-Date",
      "value": "-0.29% | HKD -92,279.64"
    },
    {
      "short": true,
      "title": "Year-to-Date",
      "value": "-0.29% | HKD 6,426,821.51"
    }
    ]
  },
  {
    "collapsed": false,
    "color": "#0000ff",
    "thumb_url2": "https://assets.bwbx.io/images/users/iqjWHBFdfxIU/iSQ_AaitARpw/v1/800x-1.jpg",
    "thumb_url": "https://image.flaticon.com/icons/svg/34/34389.svg",
    "text": "*Date & Time*: 2018-11-16 13:15\n*Location*: 1 Raffles Link\n*Agenda*: Options to buy",
    "author_name": "Appointment Request",
    "actions": [{
      "type": "button",
      "short": true,
      "text": "Accept",
      "msg_in_chat_window": true,
      "msg_processing_type": "respondWithMessage",
      "msg": "Accept!"
    }, {
      "type": "button",
      "short": true,
      "text": "Decline",
      "msg_in_chat_window": true,
      "msg_processing_type": "respondWithMessage",
      "msg": "Decline!"
    }]
  }]
  const message = {
    text: "test message",
    attachments: attachments,
  }
  await rocketChat.sendToRoom('GENERAL', message)
}

runbot()
