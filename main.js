const config = require('./config.json');
const RocketChat = require('./RocketChat');
const DialogFlow = require('./DialogFlow');

// handlers
const jokeHandler = require('./handlers/joke');
const appointmentHandler = require('./handlers/appointment');
const stockHandler = require('./handlers/stock');
const portfolioHandler = require("./handlers/portfolio");

const main = async () => {
  const rocketChat = new RocketChat(config);
  await rocketChat.connect();
  console.log('CONNECTED!');

  const dialogFlow = new DialogFlow(config);
  dialogFlow.addHandler(jokeHandler);
  dialogFlow.addHandler(appointmentHandler);
  dialogFlow.addHandler(stockHandler);
  dialogFlow.addHandler(portfolioHandler);

  rocketChat.setMessageHandler(async (message, reply) => {
    const result = await dialogFlow.processMessage(message);
    if (!result) return;

    await reply(result)
  })
};

main();
