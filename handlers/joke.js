const axios = require('axios')

module.exports = async (message, result) => {
  const intentName = result.intent.displayName
  if ( intentName !== 'Joke Intent' ) return null

  const response = await axios.get('http://api.icndb.com/jokes/random')
  const msg = response.data.value.joke
  return msg
}
