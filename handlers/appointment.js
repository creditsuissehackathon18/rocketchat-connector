const util = require('util')
const moment = require('moment')
const axios = require('axios')

module.exports = async (message, result) => {
  const intentName = result.intent.displayName
  if (intentName !== 'Meeting Request Intent') return null

  if ( result.allRequiredParamsPresent ) {
    // done!
    const dateParam = result.parameters.fields.date.stringValue
    const agendaParam = result.parameters.fields.agenda.stringValue
    const dateString = moment(dateParam).format('YYYY-MM-DD HH:mm')
    const msg = {
      attachments: [{
        title: 'This is an appointment for you',
        collapsed: false,
        color: "#0000ff",
        thumb_url: "https://image.flaticon.com/icons/svg/34/34389.svg",
        text: `*Date & Time*: ${dateString}\n*Location*: 1 Raffles Link\n*Agenda*: ${agendaParam}`,
        actions: [{
          type: "button",
          short: true,
          text: "Accept",
          msg_in_chat_window: true,
          msg_processing_type: "respondWithMessage",
          msg: "Accept!"
        }, {
          type: "button",
          short: true,
          text: "Decline",
          msg_in_chat_window: true,
          msg_processing_type: "respondWithMessage",
          msg: "Decline!"
        }]
      }]
    }
    return msg
  } else {
    const fulfillmentText = result.fulfillmentText
    return fulfillmentText
  }
}
