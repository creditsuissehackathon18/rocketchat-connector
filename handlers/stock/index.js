const axios = require('axios')
const numeral = require('numeral')
const createChart = require('./createChart')

module.exports = async (message, result) => {
  const intentName = result.intent.displayName
  if (intentName !== 'Stock Info Intent') return null

  if (result.allRequiredParamsPresent) {
    const stockParam = result.parameters.fields.stock.stringValue
    let response
    try {
      response = await axios.get(`https://api.iextrading.com/1.0/stock/${stockParam}/batch?types=chart&range=1m`)
    } catch (e) {
      return `Sorry... cannot find ${stockParam.toUpperCase()}`
    }
    const chartData = response.data.chart
    if ( !chartData.length ) return `Sorry... cannot find ${stockParam.toUpperCase()}`

    const points = chartData.map(d => ({
      t: new Date(d.date).valueOf(),
      o: d.open,
      c: d.close,
      h: d.high,
      l: d.low,
    }))
    // const points = chartData.map(d => ({ x: d.date, y: d.close }))
    const latestPrice = points[points.length - 1].c
    const latestPriceString = numeral(latestPrice).format('0,0.[00]')
    const dataUrl = await createChart(stockParam.toUpperCase(), points, 900, 500)

    const msg = {
      attachments: [{
        title: `The latest price of ${stockParam.toUpperCase()} is $${latestPriceString}`,
        image_url: dataUrl
      }]
    }

    return msg
  } else {
    const fulfillmentText = result.fulfillmentText
    return fulfillmentText
  }
}
