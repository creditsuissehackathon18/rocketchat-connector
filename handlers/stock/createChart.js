const { Chart } = require('chart.js')
require('../../vendors/chartjs-candlestick')
const { createCanvas } = require('canvas-prebuilt')

Chart.defaults.global.legend.display = false
Chart.defaults.global.responsive = true
Chart.defaults.global.maintainAspectRatio = false
Chart.pluginService.register({
  beforeDraw: function (chart, easing) {
    var ctx = chart.chart.ctx
    var chartArea = chart.chartArea
    console.log(chart)
    ctx.save()
    ctx.fillStyle = 'white'
    ctx.fillRect(0, 0, chart.width, chart.height)
    ctx.restore()
  }
})

module.exports = async (name, points, width, height) => {
  const conf = {
    type: 'candlestick',
    data: {
      datasets: [{
        color: {
          up: '#77dd77',
          down: '#ff6961',
          unchanged: '#999',
        },
        borderColor: '#000',
        borderWidth: 2,
        label: name,
        data: points,
      }],
      // labels: labels,
    },
    options: {
      title: { display: false },
      scales: {
        xAxes: [{
          offset: true,
          gridLines: {
            color: 'rgba(0, 0, 0, 0.1)'
          },
          ticks: {
            fontColor: '#000'
          }
        }],
        yAxes: [{
          offset: true,
          gridLines: {
            color: 'rgba(0, 0, 0, 0.1)'
          },
          ticks: {
            fontColor: '#000'
          }
        }]
      },
    },
  }

  const canvas = createCanvas(width, height)
  canvas.style = {}
  // Disable animation (otherwise charts will throw exceptions)
  conf.options = conf.options || {}
  conf.options.responsive = false
  conf.options.animation = false
  const context = canvas.getContext('2d')
  global.window = {}
  const chart = new Chart(context, conf)

  return new Promise((resolve, reject) => {
    chart.canvas.toDataURL('image/jpg', (err, dataUrl) => {
      if ( err ) return reject(err)
      return resolve(dataUrl)
    })
  })
}
