const numeral = require("numeral");

module.exports = (message, result) => {
  if (result.intent.displayName !== 'Portfolio Value Intent') return null;

  const portfolio = require("./test.json");
  return {
    msg: "Here's the status of your porfolio",
    attachments: [
      {
        collapsed: false,
        color: portfolio.history.mtd.value > 0 ? "#00ff00" : "#ff0000",
        title: "Total Wealth: " + portfolio.currency + " " + numeral(portfolio.value).format('0,0.[00]'),
        fields: [
          {
            short: true,
            title: "Month-to-Date",
            value: portfolio.currency + " " + numeral(portfolio.history.mtd.value).format('0,0.[00]') + " | " + ( portfolio.history.mtd.percentage * 100) + "%"
          },
          {
            short: true,
            title: "Quarter-to-Date",
            value: portfolio.currency + " " + numeral(portfolio.history.qtd.value).format('0,0.[00]') + " | " + ( portfolio.history.qtd.percentage * 100) + "%"
          },
          {
            short: true,
            title: "Year-to-Date",
            value: portfolio.currency + " " + numeral(portfolio.history.ytd.value).format('0,0.[00]') + " | " + ( portfolio.history.ytd.percentage * 100) + "%"
          },
        ]
      }
    ]
  }
}