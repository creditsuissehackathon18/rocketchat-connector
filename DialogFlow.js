const util = require('util')
const dialogflow = require('dialogflow');

class DialogFlow {
  constructor(config) {
    const { projectId } = config
    this.client = new dialogflow.SessionsClient()
    this.projectId = projectId
    this._handlers = []
  }

  addHandler(handler) {
    this._handlers.push(handler)
  }

  getSessionId(message) {
    return `session-${message.rid}-${message.u._id}`
  }

  async processMessage(message) {
    const sessionId = this.getSessionId(message)
    const sessionPath = this.client.sessionPath(this.projectId, sessionId)
    const query = message.botmsg
    const request = {
      session: sessionPath,
      queryInput: {
        text: {
          text: query,
          languageCode: 'en-US',
        }
      }
    }
    const intentResponses = await this.client.detectIntent(request)
    const queryResult = intentResponses[0].queryResult

    // match intent
    console.log('>> DialogFlow >> processing query result', util.inspect(queryResult, false, null, true /* enable colors */))
    let result = null
    for ( const handler of this._handlers ) {
      result = await handler(message, queryResult)
      if ( result ) break
    }
    return result
  }
}

module.exports = DialogFlow
